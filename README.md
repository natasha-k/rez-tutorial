# Rez Tutorial #

### What is this repository for? ###

This is a quick guide to get set up and running with Rez.

### How do I get set up? ###

* Clone the rez-tutorial repository
      
        git clone git@bitbucket.org:natasha-k/rez-tutorial.git

* Copy the .rezconfig.py file to your home directory
      
        Copy-Item .rezconfig.py -Destination ~/

* Ensure the following directories have been created
      
        ~/packages/
        ~/.rez/packages/int
        ~/.rez/packages/cache
      
* Add the Rez scripts folder to your PATH
      
        $env:PATH='C:\opt\rez\Scripts\rez\;'+$env:PATH

### Run a test ###

    $ rez --version
    Rez 2.88.0
    $ rez-config
    # Prints the rez config
    
### Run a build ###

    $ cd rez-tutorial/hello_world
    $ rez-build -i
    Invoking custom build system...
    ...
    ================================================================================
    Build Summary
    ================================================================================
    
    All 1 build(s) were successful.
    $ rez-env hello_world
    You are now in a rez-configured environment.

    resolved by N.Kelkar.Mull@Jf-Ws-H014-Mull, on Wed Jun 09 12:07:53 2021, using Rez v2.88.0
    
    requested packages:
    hello_world
    ~platform==windows  (implicit)
    ~arch==AMD64        (implicit)
    ~os==windows-10     (implicit)
    
    resolved packages:
    hello_world-1.0.0  c:\users\n.kelkar.mull\packages_test\hello_world\1.0.0
    python-3.7.4       C:\Users\N.Kelkar.Mull\.rez\packages\cache_test\python\3.7.4\f7ff\a  (cached)
    > $ hello
    Hello world!

### Run rez-gui ###

    rez-env rez PySide2 PyQt5 Qt.py -- python -m rez.cli._main gui