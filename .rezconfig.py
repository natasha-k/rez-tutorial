"""
Rez configuration settings.
"""

# flake8: noqa

import os


###############################################################################
# Paths
###############################################################################

### Do not move or delete this comment (__DOC_START__)

# The package search path. Rez uses this to find packages. A package with the
# same name and version in an earlier path takes precedence.
packages_path = [
    "~/packages",           # locally installed pkgs, not yet deployed
    "~/.rez/packages/int",  # internally developed pkgs, deployed
    "\\\mull\software\\rez-packages\ext",  # external (3rd party) pkgs, such as houdini, boost
]

# The path that Rez will locally install packages to when rez-build is used
local_packages_path = "~/packages"

# The path that Rez will deploy packages to when rez-release is used. For
# production use, you will probably want to change this to a site-wide location.
release_packages_path = "~/.rez/packages/int"

###############################################################################
# Package Caching
#
# Note: "package caching" refers to copying variant payloads to a path on local
# disk, and using those payloads instead. It is a way to avoid fetching files
# over shared storage, and is unrelated to memcached-based caching of resolves
# and package definitions as seen in the "Caching" config section.
#
###############################################################################

# Whether a package is cachable or not, if it does not explicitly state with
# the 'cachable' attribute in its package definition file. If None, defaults
# to packages' relocatability (ie cachable will == relocatable).
default_cachable = True

# Set cachable on a per-package basis. This is here for migration purposes -
# it's better for packages themselves to set their cachable attribute. Overrides
# 'default_cachable' if a package matches.
#
# Example:
#
#     default_cachable_per_package = {
#         "nuke": False,
#         "maya": True
#     }
default_cachable_per_package = None

# Set cachable on a per-package-repository basis. Overrides
# 'default_cachable_per_package' and 'default_cachable' for any repos listed here.
#
# Example:
#
#     default_cachable_per_repostitory = {
#         '/svr/packages': False
#     }
default_cachable_per_repository = None

# The path where rez locally caches variants. If this is None, then package
# caching is disabled.
cache_packages_path = "~/.rez/packages/cache"

# If True, variants in a resolve will use locally cached payloads if they are
# present in the cache.
read_package_cache = True

# If True, creating or sourcing a context will cause variants to be cached.
write_package_cache = True

# Delete variants that haven't been used in N days (see `rez-pkg-cache --clean`).
# To disable, set to zero.
package_cache_max_variant_days = 30

# Enable package caching during a package build.
package_cache_during_build = False

# Allow caching of local packages. You would only want to set this True for
# testing purposes.
package_cache_local = False

# Allow package caching if the source package is on the same physical disk
# as the package cache itself. You would only want to set this True for testing
# purposes.
package_cache_same_device = False

# If > 0, spend up to this many seconds cleaning the cache every time the cache
# is updated. This is a way to keep the cache size under control without having
# to periodically run 'rez-pkg-cache --clean'. Set to -1 to disable.
package_cache_clean_limit = 0.5

# Number of days of package cache logs to keep.
# Logs are written to {pkg-cache-root}/.sys/log/*.log
package_cache_log_days = 7


###############################################################################
# Package Resolution
###############################################################################

# Packages that are implicitly added to all package resolves, unless the
# --no-implicit flag is used.
implicit_packages = [
    "~platform=={system.platform}",
    "~arch=={system.arch}",
    "~os=={system.os}",
]

# Override platform values from Platform.os and arch.
# This is useful as Platform.os might show different
# values depending on the availability of lsb-release on the system.
# The map supports regular expression e.g. to keep versions.
# Please note that following examples are not necessarily recommendations.
#
#     platform_map = {
#         "os": {
#             r"Scientific Linux-(.*)": r"Scientific-\1",                 # Scientific Linux-x.x -> Scientific-x.x
#             r"Ubuntu-14.\d": r"Ubuntu-14",                              # Any Ubuntu-14.x      -> Ubuntu-14
#             r'CentOS Linux-(\d+)\.(\d+)(\.(\d+))?': r'CentOS-\1.\2', '  # Centos Linux-X.Y.Z -> CentOS-X.Y
#         },
#         "arch": {
#             "x86_64": "64bit",                                          # Maps both x86_64 and amd64 -> 64bit
#             "amd64": "64bit",
#         },
#     }
platform_map = {
    "os": {
        r"windows-6.1(.*)": r"windows-7",
        r"windows-6.2(.*)": r"windows-8",
        r"windows-6.3(.*)": r"windows-8.1",
        r"windows-10(.*)": r"windows-10",
    },
}

###############################################################################
# Environment Resolution
###############################################################################

# The default shell type to use when creating resolved environments (eg when using
# rez-env, or calling ResolvedContext.execute_shell). If empty or null, the
# current shell is used (for eg, "bash").
default_shell = "powershell"